"use strict";

const passwordInputs = document.querySelectorAll('input[type="password"]');
const passwordIcons = document.querySelectorAll(".icon-password");

passwordIcons.forEach((icon) => {
  icon.addEventListener("click", function () {
    const input = this.previousElementSibling;
    if (input.type === "password") {
      input.type = "text";
      this.classList.remove("fa-eye");
      this.classList.add("fa-eye-slash");
    } else {
      input.type = "password";
      this.classList.remove("fa-eye-slash");
      this.classList.add("fa-eye");
    }
  });
});

const form = document.querySelector(".password-form");
const submitButton = form.querySelector('button[type="submit"]');
submitButton.addEventListener("click", function (event) {
  event.preventDefault();
  const password1 = passwordInputs[0].value;
  const password2 = passwordInputs[1].value;
  if (password1 === password2) {
    alert("You are welcome");
  } else {
    const errorText = document.createElement("p");
    errorText.textContent = "Потрібно ввести однакові значення";
    errorText.style.color = "red";
    const parent = passwordInputs[1].parentElement;
    parent.insertBefore(errorText, passwordInputs[1].nextSibling);
  }
});
